#include "Main.h"

void usage(){
	cout << "The using format is:" << endl;
	cout << "	hlsyn cFile latency verilogFile" << endl;
}

int main(int argc, char *argv[]){
	if (argc != 4){
		usage();
		exit(1);
	}

	AssistList assistList;
	Parser::parsingCFile(string(argv[1]), assistList.portList, assistList.variableList, assistList.dependencyGraph.getOperationList());
	assistList.dependencyGraph.buildGraphConnection(assistList.portList, assistList.variableList);
	string inputLatency(argv[2]);
	int minLatency = assistList.dependencyGraph.computeMaximCycles();
	if (stoi(inputLatency) < minLatency){
		cout << "Latency cannot less than " << to_string(minLatency) << endl;
		exit(1);
	}
	assistList.dependencyGraph.setLatency(stoi(inputLatency));
	assistList.dependencyGraph.forceDirectedScheduling(assistList.timeList);
	CircuitTools::generateVerilogCode(stoi(inputLatency), assistList.portList, assistList.variableList, assistList.timeList, string(argv[3]));
	return 0;
}