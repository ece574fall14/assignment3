input UInt64 a, b, c, d, zero

output UInt64 z

variable Int64 e, f, g
variable Int1 gEQz  

e = a / b
f = c / d
g = a % b  
gEQz = g == zero
z = gEQz ? e : f 
