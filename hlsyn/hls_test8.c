input Int32 x0, x1, x2, x3, y0, c0, five, ten

output Int32 d1, d2, e, f, g, h

variable Int32 t1, t2, t3

t1 = x0 + x1
d1 = t1 * c0
d2 = d1 * five
t2 = x1 + x2
t3 = x3 + c0
e = t2 * t3
f = e * y0
g = x0 - ten
h = f + g